FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg /app/pkg/GeoLite2
WORKDIR /app/code

# renovate: datasource=github-releases depName=matomo-org/matomo versioning=semver
ARG MATOMO_VERSION=5.2.2

# https://builds.matomo.org/
RUN curl -L https://builds.matomo.org/piwik-${MATOMO_VERSION}.tar.gz | tar -xz --strip-components 1 -f - && \
    chown -R www-data:www-data /app/code

# renovate: datasource=github-tags depName=dominik-th/matomo-plugin-LoginOIDC versioning=semver
ARG OIDC_PLUGIN_VERSION=5.0.0

# install oidc plugin (https://plugins.matomo.org/LoginOIDC)
# this currently uses our custom fork - https://github.com/dominik-th/matomo-plugin-LoginOIDC/issues/95
#RUN wget https://plugins.matomo.org/api/2.0/plugins/LoginOIDC/download/${OIDC_PLUGIN_VERSION} -O /tmp/oidc.zip && \
RUN mkdir -p /app/code/plugins/LoginOIDC && \
    curl -L https://github.com/cloudron-io/matomo-plugin-LoginOIDC/archive/1470f60a90fbb409b4306b6a67bd8d3079404bc9.tar.gz | tar -xz --strip-components 1 -C /app/code/plugins/LoginOIDC -f - && \
    chown -R www-data:www-data /app/code/plugins/LoginOIDC

# The geoip plugin has the location for that hardcoded to /app/code/misc thus one can't download the location db manually.
# Doing this here pre-provisions with the free database, better than nothing for now
# We move only mmdb file because otherwise the system integrity check fails
# This used to be fetched from https://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz
ARG MAXMIND_LICENSE_KEY
ARG MAXMIND_ACCOUNT_ID
RUN cd /app/code/misc/ && \
    curl -L -u ${MAXMIND_ACCOUNT_ID}:${MAXMIND_LICENSE_KEY} https://download.maxmind.com/geoip/databases/GeoLite2-City/download?suffix=tar.gz | tar zxvf - --strip-components 1 --wildcards GeoLite2-City_*/GeoLite2-City.mmdb

# the tracker script is required to be writable (part of install checks)
RUN rm -f /app/code/config/config.ini.php && ln -s /app/data/config/config.ini.php /app/code/config/config.ini.php && \
    mv /app/code/plugins /app/pkg/plugins.orig && ln -s /app/data/plugins /app/code/plugins && \
    mv /app/code/piwik.js /app/pkg/piwik.js.orig && ln -s /app/data/piwik.js /app/code/piwik.js && \
    mv /app/code/matomo.js /app/pkg/matomo.js.orig && ln -s /app/data/matomo.js /app/code/matomo.js && \
    mv /app/code/misc/user /app/pkg/user.orig && ln -s /app/data/misc/user /app/code/misc/user && \
    rm -rf /app/code/tmp && ln -s /run/matomo/tmp /app/code/tmp

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/matomo.conf /etc/apache2/sites-enabled/matomo.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

RUN a2enmod headers expires deflate mime dir rewrite setenvif

RUN rm -rf /var/lib/php && ln -s /run/php /var/lib/php

# note without 128M the diag check complains. cli is for the cron job
RUN crudini --set /etc/php/8.3/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/8.3/cli/php.ini PHP memory_limit 768M && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.save_path /run/matomo/session && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.3/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.3/cli/conf.d/99-cloudron.ini

COPY start.sh cron.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
