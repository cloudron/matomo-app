#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    express = require('express'),
    serveStatic = require('serve-static'),
    timers = require('timers/promises'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TRACKER_PAGE_TEMPLATE = path.join(__dirname, 'trackme.ejs');
    const TRACKER_PAGE_HTML = path.join(__dirname, 'trackme.html');
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    let browser, app, localServer, cloudronName;
    let trackingEventCount = 0;

    // Keep in sync with configuration and description
    const ADMIN_USERNAME = 'admin';
    const ADMIN_PASSWORD = 'changeme';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');

        // Test server for page with tracking code
        app = express();
        app.use(serveStatic(__dirname, { }));
        localServer = app.listen(4500);
    });

    after(function () {
        browser.quit();
        localServer.close();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');

        const tmp = execSync(`cloudron exec --app ${app.id} env`).toString().split('\n').find((l) => l.indexOf('CLOUDRON_OIDC_PROVIDER_NAME=') === 0);
        if (tmp) cloudronName = tmp.slice('CLOUDRON_OIDC_PROVIDER_NAME='.length);
        // get rid of mb4 characters
        cloudronName = cloudronName.replace(/[\uD800-\uDBFF][\uDC00-\uDFFF]|[\u{10000}-\u{10FFFF}]/gu, '');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function loginAdmin() {
        await login(ADMIN_USERNAME, ADMIN_PASSWORD);
/*
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//input[@name="form_login"]'));
        await browser.findElement(By.xpath('//input[@name="form_login"]')).sendKeys(ADMIN_USERNAME);
        await browser.findElement(By.xpath('//input[@name="form_password"]')).sendKeys(ADMIN_PASSWORD);
        await browser.findElement(By.xpath('//*[@name="login_form"]')).submit();
        await waitForElement(By.xpath('//a[@title="Sign out"]'));
*/
    }

    async function login(username, password) {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//*[@name="form_login"]'));
        await browser.findElement(By.xpath('//input[@name="form_login"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="form_password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//*[@name="login_form"]')).submit();
        await browser.sleep(3000);
        await waitForElement(By.xpath('//a[@title="Sign out"]'));
    }

    async function loginOIDC(username, password, alreadyAuthenticated=true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(2000);
        await waitForElement(By.xpath('//*[@name="form_login"]'));

        await waitForElement(By.xpath(`//button[contains(., "Login with")]`));
        await browser.findElement(By.xpath(`//button[contains(., "Login with")]`)).click();
        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.sleep(2000);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await browser.sleep(3000);
        await waitForElement(By.xpath('//a[@title="Sign out"] | //a[contains(., "Sign out")]'));
    }

    async function setAccess(username, access) {
        await browser.get(`https://${app.fqdn}/index.php?module=UsersManager`);
        await waitForElement(By.xpath('//h2[contains(., "Manage Users")]'));

        await browser.findElement(By.xpath('//td[@id="userLogin" and contains(., "' + username + '")]/following-sibling::td[1]//input[contains(@class, "dropdown-trigger")]')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//td[@id="userLogin" and contains(., "' + username + '")]/following-sibling::td[1]//input[contains(@class, "dropdown-trigger")]/following-sibling::ul[1]/li[contains(., "' + access + '")]')).click();
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//div[contains(@class, "change-user-role-confirm-modal")]//div[@class="modal-footer"]/a[text()="Yes"]')).click();
        await browser.sleep(2000);
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//a[@title="Sign out"] | //a[contains(., "Sign out")]'));
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//a[@title="Sign out"] | //a[contains(., "Sign out")]')).click();
        await browser.sleep(3000);
    }

    async function checkSystem() {
        await browser.get(`https://${app.fqdn}/index.php?module=CoreAdminHome&action=home&idSite=1&period=day&date=yesterday`);
        await browser.sleep(10000);
        await waitForElement(By.xpath('//p[contains(text(), "o errors or warnings")]'));
    }

    async function fetchAndTrack() {
        fs.writeFileSync(TRACKER_PAGE_HTML, ejs.render(fs.readFileSync(TRACKER_PAGE_TEMPLATE, 'utf8').toString(), { app: app }));

        await browser.get('http://localhost:4500/trackme.html');
        await browser.wait(until.elementLocated(By.xpath(`//script[@src="https://${app.fqdn}/piwik.js"]`)), TEST_TIMEOUT);

        // give it some time to load and fire
        await browser.sleep(10000);
    }

    function waitForTrackingEvent(done) {
        function retry() {
            browser.get(`https://${app.fqdn}`).then(function () {
                return browser.wait(until.elementLocated(By.xpath('//*[@id="visitsTotal"]//tr[1]/td[3]')));
            }).then(function () {
                return browser.findElement(By.xpath('//*[@id="visitsTotal"]//tr[1]/td[3]')).getText();
            }).then(function (text) {
                if (text && parseInt(text) > trackingEventCount) {
                    console.log('Got the track: ', parseInt(text));
                    trackingEventCount = parseInt(text);
                    return done();
                }

                console.log('Event not yet present, wait a bit... text:', text);

                setTimeout(retry, 10000);
            });
        }

        retry();
    }

    function wait(done) {
        setTimeout(done, 10000);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app', async function () {
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        await timers.setTimeout(5000);
    });

    it('wait for setup', wait);

    it('can get app information', getAppInfo);
    it('cannot login as admin with wrong password', async function () {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//*[@name="form_login"]'));
        await browser.findElement(By.xpath('//input[@name="form_login"]')).sendKeys('admin');
        await browser.findElement(By.xpath('//input[@name="form_password"]')).sendKeys('password');
        await browser.findElement(By.xpath('//*[@name="login_form"]')).submit();
        await browser.sleep(3000);
        await waitForElement(By.xpath('//strong[contains(text(), "Error")]'));
    });

    it('can login with user', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD, false));
    it('can logout', logout);
    it('can login with admin', loginAdmin);
    it('can set View access to user', setAccess.bind(null, process.env.USERNAME, 'View'));
    it('checkSystem', checkSystem);
    it('can fetch tracking code', fetchAndTrack);
    it('wait for tracking event', waitForTrackingEvent);
    it('can logout', logout);

    it('can login with user', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('wait for setup', wait);

    it('can login with user', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can logout', logout);
    it('can login with admin', loginAdmin);
    it('can fetch tracking code', fetchAndTrack);
    it('wait for tracking event', waitForTrackingEvent);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('wait for setup', wait);

    it('can login with user', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can logout', logout);
    it('can login with admin', loginAdmin);
    it('checkSystem', checkSystem);
    it('can fetch tracking code', fetchAndTrack);
    it('wait for tracking event', waitForTrackingEvent);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        getAppInfo();
    });
    it('wait for setup', wait);

    it('can login with user', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can logout', logout);
    it('can login with admin', loginAdmin);
    it('checkSystem', checkSystem);
    it('can fetch tracking code', fetchAndTrack);
    it('wait for tracking event', waitForTrackingEvent);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    it('install app (no sso)', function () {
        // reset tracking count
        trackingEventCount = 0;
        execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS);
    });
    it('wait for setup', wait);
    it('can get app information', getAppInfo);
    it('can login with admin', loginAdmin);
    it('checkSystem', checkSystem);
    it('can fetch tracking code', fetchAndTrack);
    it('wait for tracking event', waitForTrackingEvent);
    it('can logout', logout);
    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', function () {
        // reset tracking count
        trackingEventCount = 0;
        execSync(`cloudron install --appstore-id org.piwik.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
    });
    it('wait for setup', wait);

    it('can get app information', getAppInfo);
    it('can login with admin', loginAdmin);
    it('can fetch tracking code', fetchAndTrack);
    it('wait for tracking event', waitForTrackingEvent);
    it('can logout', logout);

    it('can login with user', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('wait for setup', wait);

    it('can login with admin', loginAdmin);
    xit('checkSystem', checkSystem);
    it('can fetch tracking code', fetchAndTrack);
    it('tracking event still present', waitForTrackingEvent);
    it('can logout', logout);

    it('can login with user', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
