#!/bin/bash

set -eu -o pipefail

cd /app/code

readonly console='sudo -E -u www-data -- /app/code/console '
readonly crudini='sudo -E -u www-data -- crudini '

echo "=> Ensure directories"
mkdir -p /run/matomo/tmp /app/data/config /app/data/plugins /app/data/misc /run/matomo/session

# remove legacy js folder
rm -rf /app/data/js

setup() {
    while [[ ! -f "/var/run/apache2/apache2.pid" ]]; do
        echo "Waiting for apache2 to start"
        sleep 1
    done

    if [[ ! -f /app/data/config/config.ini.php ]]; then
        echo "=> Detected first run"

        echo "=> Configuring database"
        curl --fail 'http://localhost:8000/index.php?action=databaseSetup&clientProtocol=https' --data "type=InnoDB&host=mysql&username=${CLOUDRON_MYSQL_USERNAME}&password=${CLOUDRON_MYSQL_PASSWORD}&dbname=${CLOUDRON_MYSQL_DATABASE}&tables_prefix=&adapter=PDO%5CMYSQL&submit=Next+%C2%BB"

        echo "=> Creating tables"
        curl --fail -X POST 'http://localhost:8000/index.php?action=tablesCreation&clientProtocol=https&module=Installation'

        echo "=> Creating admin"
        curl --fail 'http://localhost:8000/index.php?action=setupSuperUser&clientProtocol=https&module=Installation' --data 'login=admin&password=changeme&password_bis=changeme&email=admin%40cloudron.local&submit=Next+%C2%BB'

        echo "=> Creating example website"
        curl --fail 'http://localhost:8000/index.php?action=firstWebsiteSetup&clientProtocol=https&module=Installation' --data 'siteName=Example&url=https%3A%2F%2Fwww.example.com&timezone=UTC&ecommerce=0&submit=Next+%C2%BB'

        echo "=> Finishing installation"
        curl --fail 'http://localhost:8000/index.php?action=finished&clientProtocol=https&module=Installation&site_idSite=1&site_name=Example' --data 'do_not_track=1&anonymise_ip=1&submit=Continue+to+Matomo+%C2%BB'

        echo "=> Configuring"
        $crudini --set "/app/data/config/config.ini.php" General force_ssl 1
        $crudini --set "/app/data/config/config.ini.php" General enable_update_communication 0
        $crudini --set "/app/data/config/config.ini.php" General enable_auto_update 0
        $crudini --set "/app/data/config/config.ini.php" General piwik_professional_support_ads_enabled 0
        $crudini --set "/app/data/config/config.ini.php" General 'cors_domains[]' '*'
        # required for alias domains. alternately, we can also set trusted_hosts[]
        $crudini --set "/app/data/config/config.ini.php" General enable_trusted_host_check 0

        $crudini --set "/app/data/config/config.ini.php" General "proxy_client_headers[]" "HTTP_X_FORWARDED_FOR"
        $crudini --set "/app/data/config/config.ini.php" General "proxy_host_headers[]" "HTTP_X_FORWARDED_HOST"

        # use php sessions. default is dbtable
        $crudini --set "/app/data/config/config.ini.php" General "session_save_handler" ""
        $crudini --set "/app/data/config/config.ini.php" General "enable_load_data_infile" 0

        # we run archiving via a cron job. disable browser triggered archiving
        # https://matomo.org/docs/setup-auto-archiving/#disable-browser-triggers-for-matomo-archiving-and-limit-matomo-reports-to-updating-every-hour
        mysql -u${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} -h${CLOUDRON_MYSQL_HOST} -P${CLOUDRON_MYSQL_PORT} -D${CLOUDRON_MYSQL_DATABASE} -e 'INSERT INTO `option` (option_name, option_value) VALUES ("enableBrowserTriggerArchiving", 0);'

        echo "=> Run cron job to keep system check happy"
        /app/pkg/cron.sh

        # geolocation provider. with 'geoip2php', matomo looks for specific hardcoded databases uses misc/
        mysql -u${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} -h${CLOUDRON_MYSQL_HOST} -P${CLOUDRON_MYSQL_PORT} -D${CLOUDRON_MYSQL_DATABASE} -e 'INSERT INTO `option` (option_name, option_value) VALUES ("usercountry.location_provider", "geoip2php");'

    fi

    echo "=> Updating database settings"
    # do not use the CLI tool since that itself uses the DB it seems
    $crudini --set "/app/data/config/config.ini.php" database host "${CLOUDRON_MYSQL_HOST}"
    $crudini --set "/app/data/config/config.ini.php" database port ${CLOUDRON_MYSQL_PORT}
    $crudini --set "/app/data/config/config.ini.php" database username "${CLOUDRON_MYSQL_USERNAME}"
    $crudini --set "/app/data/config/config.ini.php" database password "${CLOUDRON_MYSQL_PASSWORD}"
    $crudini --set "/app/data/config/config.ini.php" database dbname "${CLOUDRON_MYSQL_DATABASE}"

    # some existing installations have a table prefix, not sure how. maybe the users changed it
    # xargs removes the quotes
    if ! tables_prefix=$($crudini --get "/app/data/config/config.ini.php" database tables_prefix 2>/dev/null | xargs); then
        echo "=> no table prefix"
        tables_prefix=""
    else
        echo "=> table prefix:${tables_prefix}"
    fi

    echo "=> Updating email settings"
    $console config:set --section="mail" --key="defaultHostnameIfEmpty" --value="${CLOUDRON_APP_DOMAIN}"
    $console config:set --section="mail" --key="transport" --value="smtp"
    $console config:set --section="mail" --key="host" --value="${CLOUDRON_MAIL_SMTP_SERVER}"
    $console config:set --section="mail" --key="port" --value="${CLOUDRON_MAIL_SMTP_PORT}"
    $console config:set --section="mail" --key="type" --value="LOGIN"
    $console config:set --section="mail" --key="username" --value="${CLOUDRON_MAIL_SMTP_USERNAME}"
    $console config:set --section="mail" --key="password" --value="${CLOUDRON_MAIL_SMTP_PASSWORD}"
    $console config:set --section="mail" --key="encryption" --value=""

    $console config:set --section="General" --key="noreply_email_address" --value="${CLOUDRON_MAIL_FROM}"
    $console config:set --section="General" --key="login_password_recovery_email_address" --value="${CLOUDRON_MAIL_FROM}"
    $console config:set --section="General" --key="login_password_recovery_replyto_email_address" --value="${CLOUDRON_MAIL_FROM}"

    if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
        echo "=> Updating OIDC settings"

        [[ -z ${CLOUDRON_OIDC_PROVIDER_NAME} ]] && CLOUDRON_OIDC_PROVIDER_NAME="Cloudron"
        # get rid mb4 characters
        provider_name=$(php -r "echo addslashes(preg_replace('/[\xF0-\xF7].../s', '', \"${CLOUDRON_OIDC_PROVIDER_NAME}\"));")
        echo "DELETE FROM \`${tables_prefix}plugin_setting\` WHERE \`plugin_name\`='LoginOIDC' and \`user_login\`=''; INSERT INTO \`${tables_prefix}plugin_setting\` (\`plugin_name\`, \`setting_name\`,  \`setting_value\`, \`user_login\`) VALUES ('LoginOIDC','disableSuperuser','0', ''), ('LoginOIDC','disablePasswordConfirmation','1', ''), ('LoginOIDC','disableDirectLoginUrl','1', ''), ('LoginOIDC','allowSignup','1', ''), ('LoginOIDC','bypassTwoFa','1', ''), ('LoginOIDC','autoLinking','1', ''), ('LoginOIDC','authenticationName','Login with ${provider_name}', ''), ('LoginOIDC','authorizeUrl','${CLOUDRON_OIDC_AUTH_ENDPOINT}', ''), ('LoginOIDC','tokenUrl','${CLOUDRON_OIDC_TOKEN_ENDPOINT}', ''), ('LoginOIDC','userinfoUrl','${CLOUDRON_OIDC_PROFILE_ENDPOINT}', ''), ('LoginOIDC','endSessionUrl','', ''), ('LoginOIDC','userinfoId','sub', ''), ('LoginOIDC', 'useEmailAsUsername', '0', ''), ('LoginOIDC','clientId','${CLOUDRON_OIDC_CLIENT_ID}', ''), ('LoginOIDC','clientSecret','${CLOUDRON_OIDC_CLIENT_SECRET}', ''), ('LoginOIDC','scope','openid email profile', ''), ('LoginOIDC','redirectUriOverride','', ''), ('LoginOIDC','allowedSignupDomains','', '');" | mysql -v -u${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} -h${CLOUDRON_MYSQL_HOST} -P${CLOUDRON_MYSQL_PORT} -D${CLOUDRON_MYSQL_DATABASE}

        echo "=> Enable OIDC plugin"
        $console plugin:activate LoginOIDC
    fi

    # can be removed in the next release
    if [[ -z "${CLOUDRON_LDAP_SERVER:-}" ]]; then
        echo "=> Removing ldap settings"
        $console config:delete --section="LoginLdap_cloudron" --key="hostname"
        $console config:delete --section="LoginLdap_cloudron" --key="port"
        $console config:delete --section="LoginLdap_cloudron" --key="base_dn"
        $console config:delete --section="LoginLdap_cloudron" --key="admin_user"
        $console config:delete --section="LoginLdap_cloudron" --key="admin_pass"

        $console config:delete --section="LoginLdap" --key="servers[]" --value="cloudron"
        $console config:delete --section="LoginLdap" --key="ldap_user_id_field"
        $console config:delete --section="LoginLdap" --key="ldap_last_name_field"
        $console config:delete --section="LoginLdap" --key="ldap_first_name_field"
        $console config:delete --section="LoginLdap" --key="ldap_mail_field"
        $console config:delete --section="LoginLdap" --key="ldap_alias_field"
        $console config:delete --section="LoginLdap" --key="use_ldap_for_authentication"
        $console config:delete --section="LoginLdap" --key="new_user_default_sites_view_access"
        $console config:delete --section="LoginLdap" --key="synchronize_users_after_login"

        echo "=> Uninstall LDAP plugin"
        $console plugin:deactivate LoginLdap
        $console plugin:uninstall LoginLdap
    fi


    echo "=> Perform db migration"
    $console core:update --yes

    # the console CLI escapes single quotes. this in turn causes repeated invokation to keep escaping!
    # IMPORTANT: this has to be the last console call in this script for display name with single quotes to work
    # otherwise, every invokation to console escapes the single quote over and over
    $crudini --set "/app/data/config/config.ini.php" General noreply_email_name "\"${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Matomo}\""

    echo "=> Ensure permissions after setup"
    chown -R www-data:www-data /run/matomo /app/data
}

# we don't link plugins as some need write access to other depending plugins, which are built-in like the LogViewer plugin
# WARNING this does still not remove old core plugins which got removed upstream!
# I don't know how to distinguish between custom installed ones and core plugins
echo "=> Copy built-in plugins"
for dir in /app/pkg/plugins.orig/*/; do
    # for rsync to work here as we wanted the trailing slash needs to be removed
    dirWithoutSlash=${dir%/}
    echo "==> Copying ${dirWithoutSlash} ..."
    if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" || "${dirWithoutSlash}" != "LoginOIDC" ]]; then
        rsync -ac --delete "${dirWithoutSlash}" /app/data/plugins/
    fi
done

# updates from old versions require the copy even if not a new install
[[ ! -f /app/data/piwik.js ]] && cp /app/pkg/piwik.js.orig /app/data/piwik.js
[[ ! -f /app/data/matomo.js ]] && cp /app/pkg/matomo.js.orig /app/data/matomo.js

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

# This stores the custom instance logo
echo "=> Handle custom logo if any"
[[ ! -d /app/data/misc/user ]] && cp -rf /app/pkg/user.orig /app/data/misc/user

echo "=> Ensure permissions"
chown -R www-data:www-data /run/matomo /app/data

( setup ) &

echo "==> Starting matomo"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
