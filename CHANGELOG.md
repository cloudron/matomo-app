[0.1.0]
* Initial version

[0.1.1]
* Support geoip data

[0.1.2]
* First public release

[0.1.3]
* Enable archiving job through cron

[0.1.4]
* Fix username login

[0.1.5]
* Use latest SMTP variables

[0.1.6]
* Update to Piwik 2.16.1

[0.1.7]
* Use latest base image
* Update piwik to 2.16.2

[0.1.8]
* Fix screenshots

[0.1.9]
* Fix update issues

[0.2.0]
* Update post install docs

[0.2.1]
* Better post install

[0.3.0]
* Update to 2.16.5

[0.4.0]
* Update to 2.17.0

[0.4.1]
* Update to upstream 2.17.1

[0.4.2]
* Fix issue with updates and piwik.js

[0.5.0]
* Update to 3.0.0

[0.5.1]
* Update to 3.0.2
* Update to base image 0.10.0
* Pre-install a geolocation database
* Fix custom brand logo upload

[0.5.2]
* Update to 3.0.3

[0.5.3]
* Update to 3.0.3
* Use pre-built releases

[0.5.4]
* Fix built-in plugin handling

[0.5.5]
* Update to 3.0.4

[1.0.0]
* Update to 3.1.0
* Update Ldap login plugin to 4.0.2

[1.0.1]
* Update to 3.1.1

[1.1.0]
* Update to 3.2.0
* New segment editor
* Fix broken Czech language file
* Heaps more devices, and referrer spammers, are now detected by Piwik!
* https://piwik.org/changelog/piwik-3-2-0/

[1.2.0]
* Update db seed file for new installations

[1.3.0]
* Update piwik to 3.2.1
* Add documenationUrl
* https://piwik.org/changelog/piwik-3-2-1/

[1.4.0]
* Piwik is now [matomo](https://matomo.org/blog/2018/01/piwik-is-now-matomo/)
* Update matomo to 3.3.0

[1.4.0-1]
* Update matomo logo

[1.5.0]
* Update Matomo to 3.4.0

[1.6.0]
* Update Matomo to 3.5.0
* [Release blog](https://matomo.org/changelog/matomo-3-5-0/)

[1.6.0-1]
* Update Matomo to 3.5.0
* [Release blog](https://matomo.org/changelog/matomo-3-5-0/)
* Update LDAP plugin
* Give all users access to websites, by default

[1.6.1]
* Update Matomo to 3.5.1

[1.7.0]
* Update Matomo to 3.6.0

[1.8.0]
* Use new Cloudron base image

[1.8.1]
* Update Matomo to 3.6.1

[1.9.0]
* Update Matomo to 3.7.0
* Contains the new logo and branding

[1.10.0]
* Update Matomo to 3.8.0

[1.10.1]
* Update Matomo to 3.8.1

[1.11.0]
* Update Matomo to 3.9.0

[1.11.1]
* Update Matomo to 3.9.1

[1.11.2]
* Better postinstall

[1.12.0]
* Update LDAP plugin to 4.0.3
* Use manifest v2

[1.13.0]
* Use pkg convention
* Create matomo.js
* Fix most of the File integrity warnings

[1.14.0]
* Update Matomo to 3.10.0

[1.14.1]
* Fix crash when the tmp directory was cleaned up

[1.15.0]
* Update Matomo to 3.11.0

[1.15.1]
* Make misc/user writable

[1.16.0]
* Update Matomo to 3.12.0

[1.16.1]
* Fix various file permission issues
* Improve security by forcing https

[1.17.0]
* Update Matomo to 3.13.0

[1.17.1]
* Update Matomo to 3.13.1

[1.18.0]
* Use apache to serve up Matomo
* Fix issue where plugins could not be installed
* Set geolocation by default to geoip2php
* Fix email sending
* Fix system check warnings

[1.18.1]
* Update Matomo to 3.13.2

[1.18.2]
* Update Matomo to 3.13.3

[1.18.3]
* Update Matomo to 3.13.4

[1.19.0]
* Update Matomo to 3.13.5
* Update Cloudron base image to 2.0

[1.19.1]
* Update Matomo to 3.13.6

[1.19.2]
* Fix plugin handling in startup script

[1.20.0]
* Add forum url and fix screenshot links
* Make php.ini customizable via `/app/data/php.ini`

[1.21.0]
* Update Matomo to 3.14.0

[1.21.1]
* Update Matomo to 3.14.1

[1.21.2]
* Fix issue where duplicate LDAP servers were added

[1.21.3]
* Update LDAP Plugin
* Update maxmind database

[1.22.0]
* Update Matomo to 4.0.0
* [Full changelog](https://github.com/matomo-org/matomo/releases/tag/4.0.0)

[1.22.1]
* Update Matomo to 4.0.1
* [Full changelog](https://github.com/matomo-org/matomo/releases/tag/4.0.1)
* #16778 SEO Plugin broken [by @sgiehl]
* #16788 CLI Archive might not stop [by @diosmosis]
* #16786 No custom dimensions in Matomo 4.0.0

[1.22.2]
* Update Matomo to 4.0.2
* [Full changelog](https://github.com/matomo-org/matomo/releases/tag/4.0.2)

[1.22.3]
* Update Matomo to 4.0.3
* [Full changelog](https://github.com/matomo-org/matomo/releases/tag/4.0.3)

[1.22.4]
* Update Matomo to 4.0.4
* [Full changelog](https://github.com/matomo-org/matomo/releases/tag/4.0.4)

[1.22.5]
* Update Matomo to 4.0.5
* [Full changelog](https://github.com/matomo-org/matomo/releases/tag/4.0.5)

[1.23.0]
* Update Matomo to 4.1.0
* [Full changelog](https://github.com/matomo-org/matomo/releases/tag/4.1.0)

[1.23.1]
* Update Matomo to 4.1.1
* [Full changelog](https://github.com/matomo-org/matomo/releases/tag/4.1.1)

[1.24.0]
* Use base image v3
* Update PHP to 7.4
* Remove hard coded PHP memory limit setting

[1.25.0]
* Update Matomo to 4.2.0
* [Full changelog](https://github.com/matomo-org/matomo/releases/tag/4.2.0)

[1.25.1]
* Update Matomo to 4.2.1

[1.25.2]
* Disable the LOAD DATA INFILE warning shown in diagnostics view

[1.25.3]
* Update apache config to log the client IP
* Update maxmind database

[1.26.0]
* Update Matomo to 4.3.0
* [Full changelog](https://matomo.org/changelog/matomo-4-3-0/)

[1.26.1]
* Fix security issue by making various private folders actually private

[1.26.2]
* Update Matomo to 4.3.1
* [Full changelog](https://matomo.org/changelog/matomo-4-3-1/)

[1.27.0]
* Update Matomo to 4.4.0
* [Full changelog](https://matomo.org/changelog/matomo-4-4-0/)

[1.28.0]
* Update Matomo to 4.4.1
* [Full changelog](https://matomo.org/changelog/matomo-4-4-1/)
* Update maxmind database
* Optional sso support

[1.29.0]
* Update Matomo to 4.5.0
* [Full changelog](https://matomo.org/changelog/matomo-4-5-0/)
* improvements to System Checks and Reports UI
* some changes in preparation for PHP 8.1
* several security improvements including adding Content Security Policy to prevent some XSS attacks

[1.30.0]
* Update Matomo to 4.6.1
* [Full changelog](https://matomo.org/changelog/matomo-4-6-0/)
* Update LDAP plugin to 4.3.1

[1.30.1]
* Update Matomo to 4.6.2
* [Full changelog](https://matomo.org/changelog/matomo-4-6-2/)

[1.30.2]
* Update base image to 3.2.0

[1.30.3]
* Update LDAP plugin to 4.4.0

[1.31.0]
* Update Matomo to 4.7.0
* [Full changelog](https://matomo.org/changelog/matomo-4-7-0/)

[1.31.1]
* Update Matomo to 4.7.1
* [Full changelog](https://matomo.org/changelog/matomo-4-7-1/)

[1.32.0]
* Update Matomo to 4.8.0
* [Full changelog](https://matomo.org/changelog/matomo-4-8-0/)

[1.33.0]
* Update Matomo to 4.9.0
* [Full changelog](https://matomo.org/changelog/matomo-4-9-0/)

[1.33.1]
* Update Matomo to 4.9.1
* [Full changelog](https://matomo.org/changelog/matomo-4-9-1/)

[1.34.0]
* Update Matomo to 4.10.0
* [Full changelog](https://matomo.org/changelog/matomo-4-10-0/)

[1.34.1]
* Update Matomo to 4.10.1
* [Full changelog](https://matomo.org/changelog/matomo-4-10-1/)

[1.35.0]
* Update LDAP plugin to 4.5.1
* Email display name support

[1.36.0]
* Update Matomo to 4.11.0
* [Full changelog](https://matomo.org/changelog/matomo-4-11-0)

[1.36.1]
* Update LDAP plugin to 4.5.2

[1.37.0]
* Update Matomo to 4.12.0
* [Full changelog](https://matomo.org/changelog/matomo-4-12-0)
* This is a major security release. 
* Client Hints
* a new approach to allowing users to opt-out of tracking that doesn’t use iFrames
* you can now access the link to accept the invite with “Copy Link” button

[1.37.1]
* Update Matomo to 4.12.1
* [Full changelog](https://matomo.org/changelog/matomo-4-12-1)
* #19853 Goal visits log query performance improvement – don’t execute the query when no idVisits [by @tsteur, @sgiehl]
* #19819 Ensure empty report tables also use max width [by @sgiehl]
* #19838 Avoid loading goals data multiple times for goal metrics [by @sgiehl, @bx80]
* #19833 Allow empty excludedReferrers on the global setting [by @peterhashair]
* #19863 Adds missing escaping [by @sgiehl, @peterhashair]

[1.37.2]
* Update Matomo to 4.12.2
* [Full changelog](https://matomo.org/changelog/matomo-4-12-2)
* #19853 Goal visits log query performance improvement – don’t execute the query when no idVisits [by @tsteur, @sgiehl]
* #19819 Ensure empty report tables also use max width [by @sgiehl]
* #19838 Avoid loading goals data multiple times for goal metrics [by @sgiehl, @bx80]
* #19833 Allow empty excludedReferrers on the global setting [by @peterhashair]
* #19863 Adds missing escaping [by @sgiehl, @peterhashair]
* #19768 Upgrade to Matomo 4.12 breaks Matomo when DeviceDetectorCache plugin is installed [by @peterhashair]
* #19870 Fix failures link to Learn More [by @PabloCastellano, @bx80]
* #19808 Visits Overview accessibility improvement – remove redundant information [by @audrasjb, @sgiehl]

[1.37.3]
* Update Matomo to 4.12.3
* [Full changelog](https://github.com/matomo-org/matomo/releases/tag/4.12.3)

[1.37.4]
* Fix quoting when email display name has a quote

[1.38.0]
* Update Matomo to 4.13.0
* Update Cloudron base image to 4.0.0
* [Full changelog](https://github.com/matomo-org/matomo/releases/tag/4.13.0)

[1.38.1]
* Remove superfluous php-geoip configuration

[1.38.2]
* Update Matomo top 4.13.1
* [Full changelog](https://github.com/matomo-org/matomo/releases/tag/4.13.1)

[1.38.3]
* Update Matomo to 4.13.2
* [Full changelog](https://github.com/matomo-org/matomo/releases/tag/4.13.2)
* #20180 Ensure row evolution works in events when switching secondary dimension [by @sgiehl]
* #20149 When trying to find a join for segmentation, also look for available ways to join in both directions [by @diosmosis, @sgiehl]
* #19343 Further PHP 8.2 compatibility improvements [by @bx80, @sgiehl]
* #20234 Fixed PHP 8.1 deprecated warning for Redis::connect() call [by @jakeh999, @sgiehl]
* #20232 Keep selected site when returning to the dashboard from Tag Manager [by @sgiehl]
* #20233 Fixed anchor link issue on the tracking code page of the admin when clicking on MTM [by @sgiehl]
* #20256 Fix possible notice in VisitorDetails class [by @sgiehl]
* #20209 Add Snapchat to social networks [by @sgiehl]
* #20207 Added link to useful FAQ and RAW data information to Live API reference, [by @bx80]

[1.39.0]
* Update Matomo to 4.14.0
* [Full changelog](https://github.com/matomo-org/matomo/releases/tag/4.14.0)

[1.39.1]
* Update Matomo to 4.14.1
* [Full changelog](https://github.com/matomo-org/matomo/releases/tag/4.14.1)

[1.39.2]
* Update Matomo to 4.14.2
* [Full changelog](https://matomo.org/changelog/matomo-4-14-2/)
* This patch release fixes a tag manager regression discovered in Matomo 4.14.0, adds site detection for Cloudflare with a set up guide and includes detection of some new devices.

[1.40.0]
* Enable alias domains

[1.41.0]
* Update Matomo to 4.15.0
* [Full changelog](https://matomo.org/changelog/matomo-4-15-0/)
* #20785 Correctly escape report name in CSV exports [by @sgiehl]
* #12165 Submit Matomo to Google Tag Manager Community Template Galleries
* #20915 Single click copy button for code snippets [by @bx80, @sgiehl]
* #20636 [PHP 8.1 compatibility] Fix array access warning on float – /plugins/Goals/DataTable/Filter/CalculateConversionPageRate.php
* #20658 [PHP 8.1 compatibility] IPUtils.php(27): Deprecated – trim(): Passing null to parameter #1 ($string) of type string [by @sgiehl]
* #20589 Allow goals processed metrics to appear in API.getProcessedReport output [by @diosmosis, @sgiehl]
* #20606 Added GTM installation guide in no-data and tracking code pages [by @AltamashShaikh, @sgiehl]
* #20649 Added WordPress installation guide for no-data and tracking code pages [by @AltamashShaikh, @bx80]
* #20679 Fixed two issues with handling of processing dependent archives [by @diosmosis, @sgiehl]
* #20681 Don’t show empty categories in widget listing [by @diosmosis, @sgiehl]
* #20682 Allow reports to override the min/max label width values [by @diosmosis, @michalkleiner]
* #20683 add Live.initializeVisitorActions event to allow plugins to collapse actions in visitor log if they need to [by @diosmosis, @sgiehl]
* #20700 Improves UI/UX on no data screen [by @AltamashShaikh, @bx80]
* #20710 Update Google Tag Manager instructions on no data screen [by @michalkleiner, @sgiehl]
* #20748 Improved WordPress instruction tab on the no data screen [by @AltamashShaikh, @sgiehl]
* #20790 Adds Vue.js instruction tab on no data screen [by @AltamashShaikh, @sgiehl]
* #20815 Allow reports to specify a column/metadata other than “label” that uniquely identifies a row [by @diosmosis, @sgiehl]
* #20862 Improvements to notifications under tracking code on no data screen [by @AltamashShaikh, @bx80]
* #20882 Improve handling of deactivating incompatible plugins during update [by @sgiehl]
* #20890 Add instruction tab for React.js to no data screen & improve JS code instruction tab [by @AltamashShaikh, @sgiehl]
* #20928 Adds new SPA/PWA instruction tab for no data screen [by @AltamashShaikh, @sgiehl]a

[1.41.1]
* Update Matomo to 4.15.1
* [Full changelog](https://matomo.org/changelog/matomo-4-15-1/)
* #21039 There are unnecessary extra spaces in the generated campaign builder URL [by @bx80]
* #21007 Fix tag cloud visualization failing on formatted values [by @sgiehl]
* #21063 Some advanced options in tracking code generator do no longer work [by @sgiehl]

[1.41.2]
* Update base image to 4.2.0

[1.42.0]
* Migrate to OIDC login

[1.43.0]
* Update Matomo to 4.16.0
* [Full changelog](https://matomo.org/changelog/matomo-4-16-0/)
* #19617 Segment with ‘Does not contain’ operand is invalid if comma is in value [by @mneudert]
* #20959 [PHP 8.1 compatibility] Deprecated – trim(): Passing null to parameter #1 ($string) of type string [by @sgiehl]
* #21190 Fix for unexpected zero value last edit dates in segment archiving [by @bx80, @sgiehl]
* #21357 Archiving issue in DataTable.php: Deprecated – Implicit conversion from float 2009.999 to int loses precision
* #21569 Fix “revenue per visit” calculation [by @mneudert]
* #21234 Row evolution does not work for reports with special labels [by @snake14, @sgiehl]
* #21266 Process general goal metrics if no conversions, but site is ecommerce enabled [by @diosmosis, @sgiehl]
* #21403 Add markup and content for premium plugins promos [by @michalkleiner]
* #21504 Escape underscore in archive name when used in like query [by @sgiehl]
* #21512 Add more visually striking marketplace menu item in sidebar [by @michalkleiner]
* #21522 Fixing PHP 8.3 deprecation error [by @snake14, @bx80]
* #358 Add Traefik json log support [by @sethlinnkuleuven, @sgiehl]
* #359 Update test action [by @sgiehl]
* #361 Merge 4.x changes into 5.x [by @tsteur, @sgiehl]
* #362 fix nginx format detection [by @sgiehl]
* #89 Add Bluesky to social media list [by @coffeemakr, @sgiehl]
* #1367 Add urlumbrella.com [by @pableu, @spmedia]
* #1368 Add bonniesblog.online [by @ericguirbal, @spmedia]
* #1369 Ass bonniesblog.online [by @skquinn, @spmedia]
* #1370 Add jacblog.xyz [by @ericguirbal, @spmedia]
* #84 added JS option to $SUPPORTED_METHODS in opt out [by @reeno, @sgiehl]

[1.44.0]
* Update Matomo to 5.0.0
* [Full changelog](https://matomo.org/changelog/matomo-5-0-0/)
* Enhanced accessibility
* Strengthened security
* Faster insights
* Modernised front-end
* Improved compatibility

[1.44.1]
* Update Matomo to 5.0.1
* [Full changelog](https://matomo.org/changelog/matomo-5-0-1/)
* This patch fixes a regression issue when upgrading to Matomo 5 when you are using the MyISAM storage engine for MySQL 8.
* It’s also now possible to use variables in the custom variables in the tag manager.
* There have also been a number of bug fixes and performance improvements added as well.

[1.44.2]
* Detect table prefix for migrated installtions

[1.44.3]
* Update Matomo to 5.0.2
* [Full changelog](https://matomo.org/changelog/matomo-5-0-2/)
* #21809 Ensure values of fields with type password are redacted in API response [by @sgiehl]
* #21752 Remove Cloudflare from No Data Screen [by @sgiehl]
* #17807 Page Performance reports don’t show data (empty graph) when viewing performance for a flattened page URL [by @sgiehl]
* #21810 Fix possible deprecation warning in UsersManager.getUsersPlusRole API [by @sgiehl]
* #21829 [Bug] Sorting Product Revenue in Ecommerce not working [by @sgiehl]
* #21837 [Bug] Annotations get wrong date on timeline chart view [by @mneudert]
* #21595 Always show evolution of “0%” for sparklines [by @sgiehl]
* #21762 [Bug] Unable to close visitor profile by clicking ‘x’ button with one click. [by @sgiehl]
* #21769 Enable language Gujarati [by @sgiehl]
* #21800 [Bug] Console Archive Command ignores `–quiet` in v5.0.1 [by @sgiehl]

[1.44.4]
* Update Matomo to 5.0.3
* [Full changelog](https://matomo.org/changelog/matomo-5-0-3/)
* #21971 Visits log executes thousands of DB queries in a single request in getAliasSiteUrlsFromId() [by @mneudert]
* #21186 Scheduled report shows square character in HTML report in date [by @mneudert]
* #21867 [Bug] Bad date formatting in email reports [by @mneudert]
* #22001 [Bug] Call to a member function getSeriesCount() on null [by @mneudert]
* #21924 [Bug] cron:archive CLI changed response format in Matomo 5 – instead of DateTime before every row, only time is displayed [by @sgiehl]

[1.45.0]
* Make js folder writable for tag manager to work better
* Update base image to 5.0.0

[1.46.0]
* Update to 5.1.0
* [Full changelog](https://matomo.org/changelog/matomo-5-1-0/)

[1.46.1]
* Update to 5.1.1
* [Full changelog](https://matomo.org/changelog/matomo-5-1-1/)

[1.46.2]
* Update to 5.1.2
* [Full changelog](https://matomo.org/changelog/matomo-5-1-2/)

[1.47.0]
* Fix OIDC login to use username

[1.48.0]
* Update matomo to 5.2.0
* [Full Changelog](https://matomo.org/changelog/matomo-5-1-2/)
* The MultiSites API has been reworked. The previously incorrectly named metrics for the previous period now have correct names:

[1.48.1]
* CLOUDRON_OIDC_PROVIDER_NAME implemented
* checklist added
* tests updated

[1.48.2]
* Update matomo to 5.2.1
* [Full Changelog](https://matomo.org/changelog/matomo-5-1-2/)

[1.48.3]
* Update matomo to 5.2.2
* [Full Changelog](https://matomo.org/changelog/matomo-5-1-2/)
* Latest stable production release can be found at https://matomo.org/download/ ([learn more](https://matomo.org/docs/installation/)) (recommended)
* Beta and Release Candidate releases can be found at https://builds.matomo.org/ ([learn more](https://matomo.org/faq/how-to-update/faq\_159/))

[1.49.0]
* Update to correct base image 5.0.0

