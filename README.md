# Matomo Cloudron App

This repository contains the Cloudron app package source for [Matomo](http://matomo.org/).

## Installation

[![Install](https://cloudron.io/img/button32.png)](https://cloudron.io/button.html?app=org.piwik.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.piwik.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd matomo-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, backup and restore.

```
cd matomo-app/test

npm install
mocha test.js
```

## Debugging

Add this in `/app/data/config/config.ini.php`:

```
[log]
log_writers[] = file
log_level = DEBUG
```

Then, logs are in `/app/code/tmp/logs/matomo.log`
