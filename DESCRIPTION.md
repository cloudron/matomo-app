### About

Matomo provides advanced web analytics. It displays reports regarding the geographic location of visits, the source of visits (i.e. whether they came from a website, directly, or something else), the technical capabilities of visitors (browser, screen size, operating system, etc.), what the visitors did (pages they viewed, actions they took, how they left), the time of visits and more.

### Features

All standard statistics reports: top keywords and search engines, websites, top page URLs, page titles, user countries, providers, operating system, browser marketshare, screen resolution, desktop VS mobile, engagement (time on site, pages per visit, repeated visits), top campaigns, custom variables, top entry/exit pages, downloaded files, and many more, classified into four main analytics report categories – Visitors, Actions, Referrers, Goals/e-Commerce (30+ reports). See Tour of Matomo.

 * Real time data updates
Watch real time flow of visits to your website. Get a detailed view of your visitors, pages they have visited and goals they have triggered.
 * Customizable Dashboard
Create new dashboards with widget configuration fit to your needs.
 * All Websites Dashboard
Best way to get an overview of what is happening on all your websites at once.
 * Row Evolution
Current & past metric data for any row in any report.
 * Analytics for e-commerce
Understand and improve your online business thanks to advanced e-commerce analytics features.
 * Goal conversion tracking
Track Goals and identify whether you are meeting your current business objectives.
 * Event Tracking
Measure any interaction by users on your websites and apps.
 * Content Tracking
Measure impressions and clicks and CTR for image banners, text banners and any element on your pages.
 * Site Search Analytics
Track searches done on your internal search engine.
 * Custom Dimensions
Assign any custom data to your visitors or actions (like pages, events, …) and then visualize the reports of how many visits, conversions, pageviews, etc. there were for each Custom Dimension.
 * Custom Variables
similar to Custom Dimensions: custom name-value pair that you can assign to your visitors (or page views) using the JavaScript Tracking API, and then visualize the reports of how many visits, conversions, etc. for each custom variable.
 * Geolocation
Locate your visitors for accurate detection of Country, Region, City, Organization. View the visitors statistics on a World Map by Country, Region, City. View your latest visitors in real time.
 * Pages Transitions
View what visitors did before, and after viewing specific page.
 * Page Overlay
Display statistics directly on top of your website with our smart overlay.
 * Site speed & pages speed reports
Keeps track of how fast your website delivers content to your visitors.
 * Track different user interactions
Automatic tracking of file downloads, clicks on external website links, optional tracking of 404 pages
 * Analytics campaign tracking
Automatically detects Google Analytics campaign parameters in your URLs.
 * Track traffic from search engines
More than 800 different search engines tracked!
 * Scheduled email reports (PDF and HTML reports)
Embed reports in your app or website (40+ Widgets available) or embed PNG Graphs in any custom page, email, or app.
 * Annotations
Create text notes in your graphs, to remember about particular events.
 * No data limit
You can keep all your data, without any storage limits, forever!


### Administration Options

 * Manage unlimited users
 * Set user access to multiple websites
 * Manage unlimited websites
 * Exclude IPs and IP ranges
 * Exclude URL parameters
 * Timezone support, for each website
 * Supports 100+ currencies
 * Powerful options to White Label Matomo are available, ideal for large agencies or when you need a clean, branded Matomo interface
 * Upload your custom logo for UI and email reports
 * Automatically delete old logs and old data reports to keep you database size small
 * Integrates with more than 40 CMS, web frameworks or Ecommerce shops


### Tracking API

 * By default, we recommend the Javascript Tracking API
 * Mobile App Analytics with the Matomo iOS SDK, the Android SDK and the Titanium Module.
 * Asynchronous code for faster page loads
 * Track & record any data using the REST Tracking API, via the PHP Tracking API Client or the Java Tracking API client
 * Track custom page URLs and page titles
 * Track subdomains
 * User ID lets you accurately measure your unique users across multiple devices.
 * Supports both 1st party cookies (default) and 3rd party cookie (optional)
 * Import your Apache, Nginx, IIS server logs for processing in Matomo!
 * Track server speed generation time for each Page view
 * Supports pages encoded in unicode (for example, UTF-8) but also supports all other charsets


### Data Export & APIs

 *  Data Export in Excel, XML, Json and more!
 *  Fully-featured analytics API
 *  Administration API (REST) to create/update/delete pragmatically: users API, websites API, goals API, etc.
 *  Segmentation support in the API


### Log Analytics: Import and Analyze your Server Logs

 * Reprocess historical data from your server logfiles
 * Report server errors and other http status codes
 * Track and identify search engine and other bots
 * Keep your data private on your server


### User Privacy

 * Privacy compliant web analytics
 * Setting to anonymise your visitors’ IPs
 * Iframe included to let your visitor opt-out of Matomo tracking
 * DoNotTrack support by default
 * Use 1st party cookies by default, with an option to enable third party cookies
